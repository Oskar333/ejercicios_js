var elements = [
	{
		name: 'Banana',
		price: 200,
		qty: 31,
		imported: true
	},
	{
		name: 'Pomelo',
		price: 55,
		qty: 325,
		imported: false
	},
	{
		name: 'Piña',
		price: 70,
		qty: 125,
		imported: false
	},
	{
		name: 'Coco',
		price: 120,
		qty: 25,
		imported: true
	},
	{
		name: 'Papaya',
		price: 200,
		qty: 725,
		imported: true
	}
];
// 1.- Mostrar por consola todos los elementos formato objeto y formato array
// for(var i = 0; i < elements.length; i++) {
// 	console.log(elements[i]);
// }

// 2.- Mostrar por consola todos los elementos cuya cantidad sea menor de 50


// 3.- Mostrar por consola los elementos no importados


// 4.- Mostrar por consola los elementos cuyo precio sea mayor 60 y menor o igual de 120


// 5.- Mostrar por consola los elementos con precio 200 y cambiarlo a 230. 
//Mostrar nuevo array y el original
// REQUISITO: Creamos un nuevo array y guardamos todos los elementos ahí.
//No modificamos el array original
// Debemos crear nuevos objetos
var auxArray = [];
console.log("original \n");
console.log(elements);
for(var i = 0; i < elements.length; i++) {
	//Comprobar que elementos tienen precio 200
	if(elements[i].price === 200){
		//Mostrar por consola el elemento
		console.log(elements[i]);
		//Construir nuevo objeto
		var auxObject = elements[i];
		auxObject.price = 230;
		// auxObject.name = elements[i].name;
		// auxObject.price = 230;
		// auxObject.imported = elements[i].imported;
		// auxObject.qty = elements[i].qty;
		//Almacenar en el nuevo array
		auxArray.push(auxObject);
	}
}
console.log(auxArray);
console.log("final \n");
console.log(elements);







// 6.- Mostrar por consola los elementos cuyo nombre empiece por "P"
//PISTA : https://www.w3schools.com/jsref/jsref_substring.asp 