//FUNCIONES

var a = 12;
var b = 36;
var c = 3;
var opcion = true;

// 1.- Declarar una función que recibe como parámetros a y b y devuelve el resultado de su suma. Ejecutar la función.

// 2.- Declarar una función que recibe como parámetros a , b y c. Suma los elementos y devuelve la media. Ejecutar la función.

// 3.- Declarar una función que recibe como parámetros a , b , c y opcion. Ejecutar la función.
// Si opcion es true multiplica a y b y divide entre c.


var data = [
	{
		name: 'Banana',
		price: 200,
		qty: 31,
		imported: true
	},
	{
		name: 'Pomelo',
		price: 55,
		qty: 325,
		imported: false
	},
	{
		name: 'Piña',
		price: 70,
		qty: 125,
		imported: false
	}
]

//4.- 
// a) Declarar una función que recibe un objeto. 
// b) Dentro de esa función se modifica el price en base a unas tasas que son el 20%. 
// c) Devolver el objeto modificado 
// d) Declarar una función que recibe ese objeto. 
// e) Devolver el objeto modificado 
// f) Declarar otra función para que muestre por consola algo asi:
//	
//		Product: Banana
//		Price : 200
//		Checked: true
//		.................

for(var i = 0 ; i < data.length; i++) {
	var objectPriceModified = modifyPrice(data[i]);
	var object = nothingToDo(objectPriceModified);
	showElementConsole(object);
}

function modifyPrice(element) {
	element.price = 500;
	return element;
}

function nothingToDo(element) {
	return element;
}

function showElementConsole(element) {
	console.log(element.name);
	console.log(element.price);
	console.log(element.imported);
}